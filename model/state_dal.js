var mysql = require('mysql');
var express = require('express');
var router = express.Router();
var db  = require('./db_connection.js');


var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback) {
    connection.query('SELECT * FROM state;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}

exports.Insert = function(statename, callback) {
    var qry = "INSERT INTO state (statename) VALUES (?)";
    connection.query(qry, statename, function(err, result){
        callback(err, result);
    });
}


module.exports = router;

