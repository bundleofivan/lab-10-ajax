var express = require('express');
var router = express.Router();
var userDal = require('../model/user_dal');
var state_dal = require('../model/state_dal');



/* GET users listing. */


router.get('/create', function(req, res, next) {
    userDal.GetAll(function (err, result){
        if (err) {
            res.send(err);
        }
        else{
            res.render('userFormCreate.ejs', {title: 'cs355', usStates: result});
        }
    });

});



/*

router.get('/create', function(req, res, next) {
    console.log("genre: " + req.query.genrename);
    genreDal.GetAll(function (err, result){
        if (err) {
            res.send(err);
        }
        else {
            res.render('movieFormCreate.ejs', {title: 'cs355', genres: result});
        }
    });

});
*/

router.get('/all', function(req, res) {
  userDal.GetAll(function (err, result) {
        if (err) throw err;
        res.render('displayAllUsers.ejs', {rs: result});
      }
  );
});

router.get('/', function (req, res) {
    userDal.GetByID(req.query.user_id, function (err, result) {
            if (err) throw err;
            res.render('displayUserInfo.ejs', {rs: result, user_id: req.query.user_id});
        }
    );
});

router.get('/save', function(req, res, next) {
  console.log("firstname equals: " + req.query.firstname);
  console.log("the lastname submitted was: " + req.query.lastname);
  console.log("the username submitted was: " + req.query.username);
  console.log("the password submitted was: " + req.query.password);
  userDal.Insert(req.query, function(err, result){
    if (err) {
      res.send(err);
    }
    else {
      res.send("Successfully saved the user.");
    }
  });
});

router.post('/insert_user', function(req, res) {
    console.log(req.body.username, req.body.fname, req.body.lname, req.body.email, req.body.pword, req.body.state_id);
    userDal.Insert(req.body.username, req.body.fname, req.body.lname, req.body.email, req.body.pword, req.body.state_id,
        function(err){
            if(err){
                res.send('Fail!<br />' + err);
            } else {
                res.send('Success!')
            }
        });


});

router.get('/delete', function(req, res){
    console.log(req.query);
    userDal.GetByID(req.query.user_id, function(err, result) {
        if(err){
            res.send("Error: " + err);
        }
        else if(result.length != 0) {
            userDal.DeleteById(req.query.user_id, function (err) {
                res.send(result[0].fname + ' Successfully Deleted');
            });
        }
        else {
            res.send('User does not exist in the database.');
        }
    });
});




module.exports = router;
